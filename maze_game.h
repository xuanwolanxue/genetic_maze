#ifndef __MAZE_GAME_H__
#define __MAZE_GAME_H__

#include "maze_map2.h"
#include "genetic_algorithm.h"

class mazeGame: public geneticAlg<int>
{
public:
	mazeGame(mazeMap* map, int popSize, int genomeSize, int maxgenerations, double crossoverRate, double mutationRate);
	~mazeGame();
public:
	void play();
protected:
	virtual void mutate(genome<int>& chromo);

	virtual void epoch(std::vector<genome<int>>& oldPop, std::vector<genome<int>>& newPop);
private:
	void initializePop();
	void geneMutate(int gene, int geneLength, double mutationRate);
	void genomeDecode(genome<int>& chromo, std::vector<int>& moveSteps);
	void updateFitnessScore();
	double testPlay(std::vector<int>& moveSteps);
private:
	bool mIsRunning = false;
	int mPopSize;
	int mGenomeSize;
	int mGenerationCnt = 0;
	int mMaxGenerations;
	int mFittestGenomeIdx = 0;
	double mBestFitnessScore = 0.0;
	double mTotalFitnessScore = 0.0;
	mazeMap* mMap;
	std::vector<genome<int>> mPop;

};

#endif // !__MAZE_GAME_H__

