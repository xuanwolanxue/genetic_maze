
/*
 * MIT License
 * 
 * Copyright (c) 2017 wen.gu <454727014@qq.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
 
#include <stdio.h>
#include <math.h>

#include "maze_map.h"





//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

// ---- x
// |
// |
// y

#if defined(USE_MAP1)

#define ROLE_OBJ 1
#define BARRIER  2
#define MAZE_PAD 0

static int g_maze_map[MAP_HEIGHT][MAP_WIDTH] =
{
    { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 },
    { 1, 0, 2, 2, 2, 2, 0, 0, 0, 2 },
    { 2, 0, 2, 0, 0, 0, 2, 0, 0, 2 },
    { 2, 0, 0, 0, 2, 0, 2, 0, 2, 2 },
    { 2, 0, 2, 0, 0, 0, 2, 0, 0, 2 },
    { 2, 0, 0, 0, 2, 0, 0, 0, 0, 2 },
    { 2, 2, 0, 0, 0, 2, 0, 2, 0, 2 },
    { 2, 2, 0, 2, 0, 0, 0, 0, 0, 0 },
    { 2, 0, 0, 0, 2, 0, 2, 0, 0, 2 },
    { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }
};

/** the input coordinate of maze */
static const int g_maze_input_x = 0;
static const int g_maze_input_y = 1;

/** the exit coordinate of maze */
static const int g_maze_exit_x = MAP_WIDTH - 1;
static const int g_maze_exit_y = 7;

#elif defined(USE_MAP2)
#define ROLE_OBJ 5
#define BARRIER  1
#define MAZE_PAD 0

static const int g_maze_map[MAP_HEIGHT][MAP_WIDTH] =
{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1,
8, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1,
1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1,
1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1,
1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1,
1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1,
1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 5,
1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1,
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

/** the input coordinate of maze */
static const int g_maze_input_x = 14;
static const int g_maze_input_y = 7;

/** the exit coordinate of maze */
static const int g_maze_exit_x = 0;
static const int g_maze_exit_y = 2;

#endif
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


void mazeShow(const int a[MAP_HEIGHT][MAP_WIDTH])
{
    printf("\n----------------------------------------------------\n");
    int i, j;
    for (i = 0; i < MAP_HEIGHT; i++)
    {
        for (j = 0; j < MAP_WIDTH; j++)
        {
            show_obj(a[i][j]);
        }
        printf("\n\n");
    }
    printf("\n");
}

void mazeMapShow(void)
{
    mazeShow(g_maze_map);
}


void mazeArrayInitialize(int a[MAP_HEIGHT][MAP_WIDTH])
{
    int i, j;
    for (i = 0; i < MAP_HEIGHT; i++)
    {
        for (j = 0; j < MAP_WIDTH; j++)
        {
            a[i][j] = g_maze_map[i][j];
        }
    }
}


void mazeCalcCoordinate(const int a[MAP_HEIGHT][MAP_WIDTH], int direction, int* x, int* y)
{
    switch (direction)
    {
    case MV_Up:// up
        if (((*y - 1) >= 0) && (a[*y - 1][*x] != BARRIER))
        {
            (*y)--;
        }
        break;
    case MV_Down: //down
        if (((*y + 1) < MAP_HEIGHT) && (a[*y + 1][*x] != BARRIER))
        {
            (*y)++;
        }
        break;
    case MV_Left: //left
        if (((*x - 1) >= 0) && (a[*y][*x - 1] != BARRIER))
        {
            (*x)--;
        }
        break;
    case MV_Right://right
        if (((*x + 1) < MAP_WIDTH) && (a[*y][*x + 1] != BARRIER))
        {
            (*x)++;
        }
        break;
    default:
        //printf("unsupported this command:%c\n", direction);
        break;
    }
}


double mazeTestPlay(int move_steps[], int step_cnt)
{
    int i;
    int pos_x = g_maze_input_x;
    int pos_y = g_maze_input_y;
    for (i = 0; i < step_cnt; i++)
    {
        mazeCalcCoordinate(g_maze_map, move_steps[i], &pos_x, &pos_y);
    }

    //now we know the finish point of Bobs journey, let's assign
    //a fitness score which is proportional to his distance from
    //the exit

    int	diff_x = abs(pos_x - g_maze_exit_x);
    int diff_y = abs(pos_y - g_maze_exit_y);

    //we add the one to ensure we never divide by zero. Therefore
    //a solution has been found when this return value = 1
    return 1 / (double)(diff_x + diff_y + 1);
}


void mazePlay(int a[MAP_HEIGHT][MAP_WIDTH], int move_steps[], int step_cnt)
{
    int i;
    int pos_x = g_maze_input_x;
    int pos_y = g_maze_input_y;
    int role_obj = ROLE_OBJ;
    int last_value = 0;
    for (i = 0; i < step_cnt; i++)
    {
        //last_value = a[pos_y][pos_x];
        mazeCalcCoordinate(g_maze_map, move_steps[i], &pos_x, &pos_y);

        if (a[pos_y][pos_x] == 0)
        {
            a[pos_y][pos_x] = ++role_obj;
        }

        if ((pos_x == g_maze_exit_x) && (pos_y == g_maze_exit_y))
        {
            printf("route maze cost step: %d\n", i);
            break;
        }
    }
}