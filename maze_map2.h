#ifndef __MAZE_MAP2_H__
#define __MAZE_MAP2_H__

#include <vector>

enum move_step2_e
{
	MV2_Up = 0,
	MV2_Down,
	MV2_Left,
	MV2_Right,
};

class mazeMap
{
public:
	mazeMap(std::vector<std::vector<int>> map, int inputX, int inputY, int outputX, int outputY, int barrierValue, int roleValue = 1);
	~mazeMap();
public:
	void showMap();
	void showArray(std::vector<std::vector<int>>& map);
	std::vector<std::vector<int>> cloneMapArray(void);
	void testRun(std::vector<int>& moveSteps,int *endX, int* endY);

	void run(std::vector<int>& moveSteps);


	inline void getInputCoordinate(int *inputX, int* inputY)
	{
		*inputX = mInputX;
		*inputY = mInputY;
	}

	inline void getOutputCoordinate(int *outputX, int* outputY)
	{
		*outputX = mOutputX;
		*outputY = mOutputY;
	}

private:
	void calcCoordinate(std::vector<std::vector<int>>& map, int direction, int* x, int* y);
private:
	std::vector<std::vector<int>> mMapArray;
	int mInputX;
	int mInputY;
	int mOutputX;
	int mOutputY;
	int mBarrier;
	int mRole;
};

#endif // !__MAZE_MAP2_H__

