/*
 * MIT License
 * 
 * Copyright (c) 2017 wen.gu <454727014@qq.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef __MAZE_MAP_H__
#define __MAZE_MAP_H__

#ifdef __cplusplus
extern "C" {
#endif // !__cplusplus

#define USE_MAP1 1
//#define USE_MAP2 1

#if defined(USE_MAP1)

#define MAP_HEIGHT  10
#define MAP_WIDTH   10

#elif defined(USE_MAP2)

#define MAP_HEIGHT  10
#define MAP_WIDTH   15

#endif
	

	




#define show_obj(obj) printf("%4d ", obj)


enum move_step_e 
{
    MV_Up = 0,
    MV_Down,
    MV_Left,
    MV_Right,
};


/**
*
*@brief  show all element of 2D array.
*
*@param  a [in] the 2D array.
*
*@return None.
*
*@see
*/
void mazeShow(const int a[MAP_HEIGHT][MAP_WIDTH]);

/**
*
*@brief  show map.
*
*@param  none
*
*@return None.
*
*@see
*/
void mazeMapShow(void);

/**
*
*@brief  initialize maze ai data array with a 2D array.
*
*@param  a [in] the 2D array.
*
*@return None.
*
*@see
*/
void mazeArrayInitialize(int a[MAP_HEIGHT][MAP_WIDTH]);




/**
*
*@brief  try to route maze
*
*@param  move_steps [in] a array for move steps.
*@param  step_cnt   [in] the count of step in array
*
*@return fitness(distance from exit).
*
*@see
*/
double mazeTestPlay(int move_steps[], int step_cnt);

/**
*
*@brief  try to route maze
*
*@param  a [in] the 2D array.
*@param  move_steps [in] a array for move steps.
*
*@return None.
*
*@see
*/
void mazePlay(int a[MAP_HEIGHT][MAP_WIDTH], int move_steps[], int step_cnt);



#ifdef __cplusplus
}
#endif // !__cplusplus


#endif // !__MAZE_MAP_H__

