/*
 * MIT License
 * 
 * Copyright (c) 2017 wen.gu <454727014@qq.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "maze_map.h"
#include "maze_genetic.h"


static int g_maze_ai_data[MAP_HEIGHT][MAP_WIDTH];

void test_maze_c()
{
	int generation = 0;
	int max_generation_cnt = 500; /** for avoid endless loop */
	population_t population;
	srand((unsigned)time(NULL)); /** 用时间做种，每次产生随机数不一样 */

	mazeMapShow();
	mazeArrayInitialize(g_maze_ai_data);

	populationInitialize(&population, POPULATION_SIZE, GENE_LENGTH, CHROMOSOME_LENGTH, CROSSOVER_RARE, MUTATION_RATE);

	while (generation < max_generation_cnt)
	{
		if (populationEpoch(&population) == 1) /** mean that got fittest genome */
		{
			break;
		}

		generation++;
	}


	if (population.is_running == 0)
	{
		int move_steps[CHROMOSOME_LENGTH];

		genomeDecode(&population.genomes[population.fittest_genome], move_steps, CHROMOSOME_LENGTH);
		mazePlay(g_maze_ai_data, move_steps, CHROMOSOME_LENGTH);
		mazeShow(g_maze_ai_data);
	}
	else
	{
		printf("not found fittest genome\n");
	}

	printf("genetic generation: %d\n", population.generation);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

#include <vector>

using namespace std;

#if 0
static const vector<vector<int>> g_map =
{
	{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 },
	{ 5, 0, 2, 2, 2, 2, 0, 0, 0, 2 },
	{ 2, 0, 2, 0, 0, 0, 2, 0, 0, 2 },
	{ 2, 0, 0, 0, 2, 0, 2, 0, 2, 2 },
	{ 2, 0, 2, 0, 0, 0, 2, 0, 0, 2 },
	{ 2, 0, 0, 0, 2, 0, 0, 0, 0, 2 },
	{ 2, 2, 0, 0, 0, 2, 0, 2, 0, 2 },
	{ 2, 2, 0, 2, 0, 0, 0, 0, 0, 8 },
	{ 2, 0, 0, 0, 2, 0, 2, 0, 0, 2 },
	{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }
};

/** the input coordinate of maze */
static const int g_maze_input_x = 0;
static const int g_maze_input_y = 1;
static const int g_maze_barrier = 2;
static const int g_maze_role = 5;

/** the exit coordinate of maze */
static const int g_maze_exit_x = g_map[0].size() - 1;
static const int g_maze_exit_y = 7;

#else
static const vector<vector<int>> g_map =
{
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1 },
	{ 8, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1 },
	{ 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1 },
	{ 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1 },
	{ 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1 },
	{ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1 },
	{ 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 5 },
	{ 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
};

/** the input coordinate of maze */
static const int g_maze_input_x = 14;
static const int g_maze_input_y = 7;
static const int g_maze_barrier = 1;
static const int g_maze_role = 5;

/** the exit coordinate of maze */
static const int g_maze_exit_x = 0;
static const int g_maze_exit_y = 2;

#endif

#include "maze_map.h"
#include "maze_game.h"

static const int g_pop_size = 140;
static const int g_genome_size = 35;
static const int g_max_generations = 500;
static const double g_crossover_rate = 0.7;
static const double g_mutation_rate = 0.001;


void test_maze_cpp()
{
	mazeMap myMap(g_map, g_maze_input_x, g_maze_input_y, g_maze_exit_x, g_maze_exit_y, g_maze_barrier, g_maze_role);
	mazeGame myGame(&myMap, g_pop_size, g_genome_size, g_max_generations, g_crossover_rate, g_mutation_rate);

	myMap.showMap();

	myGame.play();
}

int main(int argc, char* argv[])
{
   
	test_maze_c();
	test_maze_cpp();

    getchar();
    return 0;
}
